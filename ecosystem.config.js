module.exports = {
  apps : [{
    name   : "meat-api",
    script : "./dist/main.js",
    instances: 0,
    exec_mode: "cluster",
    watch: true, //pode ser um array com os paths que se quer monitorar
    env: {
      SERVER_PORT: 5000,
      DB_URL: 'mongodb://localhost:27017/comics-universe',
      NODE_ENV: 'development'
    },
    env_production: {
      SERVER_PORT: 5001,
      DB_URL: 'mongodb://localhost:27017/comics-universe',
      NODE_ENV: "production"
    }

  }]
}
