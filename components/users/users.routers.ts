import * as restify from 'restify';
import { Ouser} from './users.model';
import { ModelRouter } from '../../common/router-model';
import { authenticate } from '../../common/security/auth.handler';
import { authorize } from '../../common/security/autz.handler';

class UsersRouter extends ModelRouter<Ouser>{

  constructor() {
    super(Ouser);
    this.on('beforeRender', document => {
      document.password = undefined;
    });
  }

  findByEmail = (req, resp, next) => {
    if (req.query.email) {
      Ouser.findByEmail(req.query.email)
        .then(user => user ? [user] : [])
        .then(this.renderAll(resp, next, {
          pageSize: this.pageSize,
          url: req.url
        }))
        .catch(next);
    } else {
      next();
    }
  }

  applyRoutes(app: restify.Server) {
    app.get(`${this.basePath}`, [authorize('admin'),this.findByEmail,this.findAll]);
    app.get(`${this.basePath}/:id`, [authorize('admin'),this.findById]);
    app.post(`${this.basePath}`, [authorize('admin'),this.save]);
    app.put(`${this.basePath}/:id`, [authorize('admin'),this.replace]);
    app.patch(`${this.basePath}/:id`, [authorize('admin'),this.update]);
    app.del(`${this.basePath}/:id`, [authorize('admin'),this.delete]);
    //app.del('/ouser/:id', this.delete);

    app.post(`${this.basePath}/authenticate`, authenticate);
  }
}

export const usersRouter = new UsersRouter();