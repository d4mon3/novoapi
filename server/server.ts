import * as fs from 'fs';
import * as restify from 'restify';
import { enviroment } from '../common/enviroments';
import { Router } from '../common/router';
import * as mongoose from 'mongoose';
import { mergePatchBodyParser } from './merge-patch.parser';
import { handleError } from './error.handler';
import { tokenParser } from '../common/security/token.parser';
import { logger } from '../common/logger';

export class Server {

  application: restify.Server;

  initializeDb(){
    return mongoose.connect(enviroment.db.url, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useFindAndModify: true,
    });  
    
  }

  initRoutes(routers: Router[]): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        this.application = restify.createServer({
          name: 'meat-api',
          version: '1.0.0',
          log: logger
          //certificate: fs.readFileSync('../common/security/keys/cert.pem'),
          //key: fs.readFileSync('../common/security/keys/key.pem')
        });

        this.application.pre(restify.plugins.requestLogger({
          log: logger
        }));

        this.application.use(restify.plugins.queryParser());
        this.application.use(restify.plugins.bodyParser());
        this.application.use(mergePatchBodyParser);
        this.application.use(tokenParser);

        for (let router of routers) {
          router.applyRoutes(this.application);
        }

        this.application.listen(enviroment.server.port, () => {
          resolve(this.application);
        });

        this.application.on('restifyError', handleError);

        // não deixar a rotina abaixo rodando em produção. Expoe dados de login e token
        //this.application.on('after', restify.plugins.auditLogger({
        //  log: logger,
        //  event: 'after',
        //  server: this.application
        //}));
        
      } catch (error) {
        reject(error);
      }
    });
  }

  bootstrap(routers: Router[] = []): Promise<Server> {
    return this.initializeDb().then(() =>
      this.initRoutes(routers).then(() => this));
  }
}